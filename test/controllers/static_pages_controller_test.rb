#Made by Augustinas Cerniauskas
require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "One Pound Coffee"
  end

  #test "should get root" do
  #  get FILL_IN
  #  assert_response FILL_IN
  #end
  
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "Home | #{@base_title}"
  end

  test "should get regions" do
    get regions_path
    assert_response :success
    assert_select "title", "Regions | #{@base_title}"
  end

  test "should get types" do
    get types_path
    assert_response :success
    assert_select "title", "Types | #{@base_title}"
  end

  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact | #{@base_title}"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | #{@base_title}"
  end

end
