#Made by Augustinas Cerniauskas, edit by Augustinas Cerniauskas and Tautvydas Cerniauskas
Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  
  get 'sessions/new'

  get 'users/new'

  #get '/regions', to: 'static_pages#regions'
  
  get '/types', to: 'static_pages#types'

  get '/contact', to: 'static_pages#contact'

  get '/about', to: 'static_pages#about'
  
  get  '/signup',  to: 'users#new'
  
  post '/signup',  to: 'users#create'
  
  get '/login', to: 'sessions#new'
  
  post '/login', to: 'sessions#create'
  
  delete '/logout', to: 'sessions#destroy'
  
  resources :places, except: [:update, :edit, :destroy]
  
  get '/regions', to: 'places#index'
  
  resources :users

  root 'static_pages#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
