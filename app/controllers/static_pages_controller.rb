class StaticPagesController < ApplicationController
  def home
  end

  def regions
  end

  def types
  end

  def contact
  end

  def about
  end
end
